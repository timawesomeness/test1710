package com.timawesomeness.dev.minecraft.test1710;

import javax.swing.Icon;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockController extends Block {

	protected BlockController() {
		super(Material.iron);
	}
	
	@SideOnly(Side.CLIENT)
	public static IIcon sideIcon;
	public static IIcon topBottomIcon;
	
	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
	    sideIcon = par1IconRegister.registerIcon("test1710:a");
	    topBottomIcon = par1IconRegister.registerIcon("test1710:b");
	}
	       
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata) {
	        if(side == 0 || side == 1) {
	                return topBottomIcon;
	        } else {
	                return sideIcon;
	        }
	}

}
