package com.timawesomeness.dev.minecraft.test1710;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = Test1710.MODID, name = "test1710", version = Test1710.VERSION)
public class Test1710 {
	public static final String MODID = "test1710";
	public static final String VERSION = "1.0";

	public static Block chicken;
	public static Block UDCcontroller;
	public static Block multiSidedBlock;
	public static Item testItem;

	@EventHandler
	public void preInit(FMLInitializationEvent event) {
		testItem = new Item().setUnlocalizedName("itemTest").setCreativeTab(UDCtab);
		GameRegistry.registerItem(testItem, "Test Item");
	}

	public static CreativeTabs UDCtab = new CreativeTabs("UDCcablesTab") {
		public Item getTabIconItem() {
			return Items.hopper_minecart;
		}
	};
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		chicken = new BlockChicken(Material.rock).setCreativeTab(UDCtab).setBlockName("chicken");//.setHarvestLevel("pickaxe", 1);
		chicken.setHarvestLevel("pickaxe", 0);
		GameRegistry.registerBlock(chicken, "Chicken");
		LanguageRegistry.addName(chicken, "Chicken");
		
		UDCcontroller = new BlockController().setBlockName("UDCterminal").setCreativeTab(UDCtab);
		UDCcontroller.setHarvestLevel("pickaxe", 0);
		GameRegistry.registerBlock(UDCcontroller, "UDC terminal");
		LanguageRegistry.addName(UDCcontroller, "UDC Control Terminal");
		
		multiSidedBlock = new MultiSidedBlock(Material.rock).setCreativeTab(UDCtab);
		multiSidedBlock.setHarvestLevel("pickaxe", 1);
		GameRegistry.registerBlock(multiSidedBlock, "multiSidedBlock");
		LanguageRegistry.addName(multiSidedBlock, "MultiSidedBlock");
	}
}
