package com.timawesomeness.dev.minecraft.test1710;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class MultiSidedBlock extends Block {

	private IIcon iconFront;
	private IIcon iconTopBottom;
	
	protected MultiSidedBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister) {
		blockIcon = iconRegister.registerIcon("test1710:a");
		iconFront = iconRegister.registerIcon("test1710:chicken");
		iconTopBottom = iconRegister.registerIcon("test1710:b");
	}
	
	//Sides: 0=bottom 1=top 2-5=sides
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata){
			return (side == 3 && metadata == 0 ? iconFront : (side == 1 ? iconTopBottom : (side == 0 ? iconTopBottom : side != metadata ? blockIcon : iconFront)));
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entityliving, ItemStack stack) {
		int l = MathHelper.floor_double((double)(entityliving.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

        if (l == 0)
        {
            world.setBlockMetadataWithNotify(x, y, z, 2, 2);
        }

        if (l == 1)
        {
            world.setBlockMetadataWithNotify(x, y, z, 5, 2);
        }

        if (l == 2)
        {
            world.setBlockMetadataWithNotify(x, y, z, 3, 2);
        }

        if (l == 3)
        {
            world.setBlockMetadataWithNotify(x, y, z, 4, 2);
        }
	}
	
}
